<?php
/**
 * Created by IntelliJ IDEA.
 * User: cadelgado
 * Date: 13/12/15
 * Time: 21:58
 */
$tamarindo = file_get_contents("http://tamarindoexpress.com/tasas-de-cambio/");
$strVzla = 'Venezuela: ';
$idxVzla = stripos($tamarindo, $strVzla);
$tamarindoVal = substr($tamarindo, $idxVzla + strlen($strVzla), 6);
$tamarindoVal = str_replace(",", ".", $tamarindoVal);


$result['tamarindo'] = (float) $tamarindoVal;

$smallworldEUR = file_get_contents("http://www.smallworldfs.com/payoutnetwork/calculate?importe=1&method=BANK_DEPOSIT&countryOrigin=ESP&currencieOrigin=EUR&countryDestination=VEN&currencieDestination=VEF&decimal=.");
$smallworldEURVal = json_decode($smallworldEUR);


$result['smallworld']['EUR'] = (float)$smallworldEURVal->rate;

$smallworldUSD = file_get_contents("http://www.smallworldfs.com/payoutnetwork/calculate?importe=100&method=BANK_DEPOSIT&countryOrigin=USA&currencieOrigin=USD&countryDestination=VEN&currencieDestination=VEF&decimal=.");
$smallworldUSDVal = json_decode($smallworldUSD);

$result['smallworld']['USD'] = (float)$smallworldUSDVal->rate;

$localbitcoins = file_get_contents("https://localbitcoins.com/bitcoincharts/VEF/orderbook.json");
$localbitcoinsVal = json_decode($localbitcoins);

$result['localbitcoins'] = $localbitcoinsVal;

$uphold = file_get_contents("https://api.uphold.com/v0/ticker/USD");
$upholdVal = json_decode($uphold);

$result['uphold'] = $upholdVal;

$bitinka = file_get_contents("https://www.bitinka.pe/api/apinka/ticker");
$bitinkaVal =  new SimpleXMLElement($bitinka);
$result['bitinka'] = $bitinkaVal;

header('Content-type: application/json');
echo json_encode($result);
