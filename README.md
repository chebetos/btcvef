# README - LEEME #

Este proyecto quiere contribuir a proporcionar datos con respecto a la cotización del Bitcoin en el mercado Venezolano y el mercado internacional, así como comparativas y estadísticas sobre las mismas. 

## ¿Cómo ha sido implementado y porqué? ##

En su versión inicial, este proyecto busca poder ejecutarse de forma autónoma en cualquier navegador y sin depender de un servidor, por ello está implementado en HTML y JavaScript y obteniendo los datos directamente de los sitios de intercambio (exchanges). 

Aquellos que presentan algún problema para ser consultados, se proporciona un programa PHP que sirve de intermediario.

## ROADMAP - Mapa de Ruta ##

* Estadísticas históricas.
* Comparativas de diferencias porcentuales. 
* Gráficas de tasa media diaria, tanto lineal como de velas.
* Implementar como extensión de Chrome y Firefox. Lo cual ayudaría a su distribución y actualización, sin que afecte el bloqueo de un servidor.